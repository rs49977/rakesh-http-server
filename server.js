const http = require("http");
const path = require("path");
const fs = require("fs");

// obj of status code, description pair
const statusCodes = http.STATUS_CODES;

// port number of the server
const port = 8000;

// request handler of the server
const requestListner = (req, res) => {
    const url = req.url;

    if(url === "/") {
        // home page msg
        res.write("Hii, welcome to my Node.js server");
        res.end();

    } else if( url === "/html") {
        // writing html file in res
        const filePath = path.resolve(__dirname, "./public/index.html");
        fs.readFile(filePath, "utf8", (err, data) => {
            if(err) {
                res.writeHead(404);
                res.write("File not found!")
            } else {
                res.writeHead(200, { "Content-Type": "text/html" });
                res.write(data);
            }
            res.end();
        })

    } else if ( url === "/json") {
        // wrting json data in res
        const filePath = path.resolve(__dirname, "./public/data.json");
        fs.readFile(filePath, "utf8", (err, data) => {
            if(err) {
                res.writeHead(404);
                res.write("File not found!")
            } else {
                res.writeHead(200, { "Content-Type": "application/json" });
                res.write(data);
            }
            res.end();
        })

    } else if( url === "/uuid") {
        // write uuid in res
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(`{ "uuid": "14d96bb1-5d53-472f-a96e-b3a1fa82addd" }` );
        res.end();

    } else if ( url.includes("/status")) {
        // get status code from req url
        const status = url.split("/").slice(-1)[0];
        const des = statusCodes[status];
        
        if(des) {
            res.writeHead(status);
            res.write(des);
        } else {
            // for invalid req status code
            res.writeHead(404);
            res.write("status code not found!")
        }
        res.end();

    } else if( url.includes("/delay")) {
        // delay time in sec
        const delayTime = url.split("/").slice(-1)[0];

        if(delayTime) {
            // wait till delay time then send res
            setTimeout(() => {
                res.writeHead(200);
                res.write(statusCodes[200]);
                res.end();
            }, delayTime * 1000)

        } else {
            // for invalid time 
            res.writeHead(404);
            res.write("Time not valid!")
            res.end();

        }
    } else {
        // for not found url
        res.writeHead(404);
        res.write("Page not found!")
        res.end();
    }

    
}


// http server to handle client request
const server = http.createServer(requestListner);
server.listen(port, err => {
    const msg = err ?  "Somethig went wrong =>"+err : "Server running on port: "+port;
    console.log(msg);
})